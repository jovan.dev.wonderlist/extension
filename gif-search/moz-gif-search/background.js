
function onCreated() {
  if (browser.runtime.lastError) {
    console.log(`Error: ${browser.runtime.lastError}`);
  } else {
    console.log("Item created successfully");
  }
}

function onRemoved() {
  console.log("Item removed successfully");
}

function onError(error) {
  console.log(`Error: ${error}`);
}

browser.menus.create({
  id: "search-giphy",
  title: 'Giphy',
  contexts: ["selection"]
}, onCreated);

browser.menus.create({
  id: "search-tenor",
  title: 'Tenor',
  contexts: ["selection"]
}, onCreated);

browser.menus.create({
  id: "search-imgur",
  title: 'Imgur',
  contexts: ["selection"]
}, onCreated);

browser.menus.onClicked.addListener((maindo, tab) => {
  switch (maindo.menuItemId) {
    case "search-giphy":
      console.log(maindo.selectionText);
      var yois = browser.tabs.create({
        url: "https://giphy.com/search/"+maindo.selectionText
      });
      yois.then(onCreated, onError);
      break;
    case "search-tenor":
      console.log(maindo.selectionText);
      var yois = browser.tabs.create({
        url: "https://tenor.com/search/"+maindo.selectionText
      });
      yois.then(onCreated, onError);
      break;
    case "search-imgur":
      console.log(maindo.selectionText);
      var yois = browser.tabs.create({
        url: "https://imgur.com/search?q="+maindo.selectionText
      });
      yois.then(onCreated, onError);
      break;
  }
});
