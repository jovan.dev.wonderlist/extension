
function onCreated() {
  if (browser.runtime.lastError) {
    console.log(`Error: ${browser.runtime.lastError}`);
  } else {
    console.log("Item created successfully");
  }
}

function onRemoved() {
  console.log("Item removed successfully");
}

function onError(error) {
  console.log(`Error: ${error}`);
}

browser.menus.create({
  id: "search-newonnetflix",
  title: 'Newonnetflix',
  contexts: ["selection"]
}, onCreated);

browser.menus.create({
  id: "search-hbogo",
  title: 'Hbogo',
  contexts: ["selection"]
}, onCreated);

browser.menus.create({
  id: "search-Disney",
  title: 'Disney',
  contexts: ["selection"]
}, onCreated);

browser.menus.create({
  id: "search-hulu",
  title: 'Hulu',
  contexts: ["selection"]
}, onCreated);

browser.menus.create({
  id: "search-imdb",
  title: 'Imdb',
  contexts: ["selection"]
}, onCreated);

browser.menus.create({
  id: "search-rottentomatoes",
  title: 'Rottentomatoes',
  contexts: ["selection"]
}, onCreated);

browser.menus.onClicked.addListener((shiyoho, tab) => {
  switch (shiyoho.menuItemId) {
    case "search-newonnetflix":
      console.log(shiyoho.selectionText);
      var yubi = browser.tabs.create({
        url: "https://usa.newonnetflix.info/catalog/search/"+shiyoho.selectionText+"#results"
      });
      yubi.then(onCreated, onError);
      break;
    case "search-hbogo":
      console.log(shiyoho.selectionText);
      var yubi = browser.tabs.create({
        url: "https://hbogo.me/search/"+shiyoho.selectionText
      });
      yubi.then(onCreated, onError);
      break;
    case "search-Disney":
      console.log(shiyoho.selectionText);
      var yubi = browser.tabs.create({
        url: "https://www.google.com/search?q="+shiyoho.selectionText+"+site%3Adisneyplus.com"
      });
      yubi.then(onCreated, onError);
      break;
    case "search-hulu":
      console.log(shiyoho.selectionText);
      var yubi = browser.tabs.create({
        url: "https://www.google.com/search?q="+shiyoho.selectionText+"+site%3Ahulu.com"
      });
      yubi.then(onCreated, onError);
      break;
    case "search-imdb":
      console.log(shiyoho.selectionText);
      var yubi = browser.tabs.create({
        url: "https://www.imdb.com/find?q="+shiyoho.selectionText
      });
      yubi.then(onCreated, onError);
      break;
    case "search-rottentomatoes":
      console.log(shiyoho.selectionText);
      var yubi = browser.tabs.create({
        url: "https://www.rottentomatoes.com/search?search="+shiyoho.selectionText
      });
      yubi.then(onCreated, onError);
      break;
  }
});
