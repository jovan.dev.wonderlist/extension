
function genericOnClick(info, tab) {
  console.log(info.selectionText)
  if (info.menuItemId == 1) {
    chrome.tabs.create({
      url: "https://www.youtube.com/results?search_query="+info.selectionText+"&page={startPage?}&utm_source=opensearch"
    });
  } else if (info.menuItemId == 2) {
    chrome.tabs.create({
      url: "https://vimeo.com/search?q="+info.selectionText
    });
  }
}

var titles = ['Youtobu', 'Vimeo']
for (var i = 0; i < titles.length; i++) {
  var title = titles[i]
  var id = chrome.contextMenus.create({"title": title, "contexts":['selection'], "onclick": genericOnClick});
}
