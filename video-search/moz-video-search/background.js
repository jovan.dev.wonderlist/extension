
function onCreated() {
  if (browser.runtime.lastError) {
    console.log(`Error: ${browser.runtime.lastError}`);
  } else {
    console.log("Item created successfully");
  }
}

function onRemoved() {
  console.log("Item removed successfully");
}

function onError(error) {
  console.log(`Error: ${error}`);
}

browser.menus.create({
  id: "search-youtube",
  title: 'search in youtobe.com',
  contexts: ["selection"]
}, onCreated);

browser.menus.create({
  id: "search-vimeo",
  title: 'search in vimeo.com',
  contexts: ["selection"]
}, onCreated);

browser.menus.onClicked.addListener((info, tab) => {
  switch (info.menuItemId) {
    case "search-youtube":
      console.log(info.selectionText);
      var sakusei = browser.tabs.create({
        url: "https://www.youtube.com/results?search_query="+info.selectionText+"&page={startPage?}&utm_source=opensearch"
      });
      sakusei.then(onCreated, onError);
      break;
    case "search-vimeo":
      console.log(info.selectionText);
      var sakusei = browser.tabs.create({
        url: "https://vimeo.com/search?q="+info.selectionText
      });
      sakusei.then(onCreated, onError);
      break;
  }
});
