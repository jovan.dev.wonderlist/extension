
function genericOnClick(arei, tab) {
  console.log(arei.selectionText)
  if (arei.menuItemId == 1) {
    chrome.tabs.create({
      url: "https://translate.google.com/?sl=auto&tl=en&text="+arei.selectionText+"&op=translate"
    });
  } else if (arei.menuItemId == 2) {
    chrome.tabs.create({
      url: "https://www.bing.com/translator/?text="+arei.selectionText
    });
  } else if (arei.menuItemId == 3) {
    chrome.tabs.create({
      url: "https://www.wordreference.com/es/translation.asp?tranword="+arei.selectionText
    });
  } else if (arei.menuItemId == 4) {
    chrome.tabs.create({
      url: "https://www.deepl.com/en/translator#en/de/"+arei.selectionText
    });
  }
}

var namae = ['Google', 'Bing', 'Wordreference', 'Deepl']
for (var i = 0; i < namae.length; i++) {
  var title = namae[i]
  var id = chrome.contextMenus.create({"title": title, "contexts":['selection'], "onclick": genericOnClick});
}
