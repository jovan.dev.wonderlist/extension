
function onCreated() {
  if (browser.runtime.lastError) {
    console.log(`Error: ${browser.runtime.lastError}`);
  } else {
    console.log("Item created successfully");
  }
}

function onRemoved() {
  console.log("Item removed successfully");
}

function onError(error) {
  console.log(`Error: ${error}`);
}

browser.menus.create({
  id: "search-unsplash",
  title: 'Unsplash',
  contexts: ["selection"]
}, onCreated);

browser.menus.create({
  id: "search-creativecommons",
  title: 'Creative Commons',
  contexts: ["selection"]
}, onCreated);

browser.menus.create({
  id: "search-pinterest",
  title: 'Pinterest',
  contexts: ["selection"]
}, onCreated);

browser.menus.create({
  id: "search-pexels",
  title: 'Pexels',
  contexts: ["selection"]
}, onCreated);

browser.menus.create({
  id: "search-pixabay",
  title: 'PixaBay',
  contexts: ["selection"]
}, onCreated);

browser.menus.onClicked.addListener((chosa, tab) => {
  switch (chosa.menuItemId) {
    case "search-unsplash":
      console.log(chosa.selectionText);
      var otoko = browser.tabs.create({
        url: "https://unsplash.com/s/photos/"+chosa.selectionText
      });
      otoko.then(onCreated, onError);
      break;
    case "search-creativecommons":
      console.log(chosa.selectionText);
      var otoko = browser.tabs.create({
        url: "https://creativecommons.org/?s="+chosa.selectionText
      });
      otoko.then(onCreated, onError);
      break;
    case "search-pinterest":
      console.log(chosa.selectionText);
      var otoko = browser.tabs.create({
        url: "https://www.pinterest.com/search/pins/?q="+chosa.selectionText
      });
      otoko.then(onCreated, onError);
      break;
    case "search-pexels":
      console.log(chosa.selectionText);
      var otoko = browser.tabs.create({
        url: "https://www.pexels.com/search/"+chosa.selectionText
      });
      otoko.then(onCreated, onError);
      break;
    case "search-pixabay":
      console.log(chosa.selectionText);
      var otoko = browser.tabs.create({
        url: "https://pixabay.com/images/search/"+chosa.selectionText
      });
      otoko.then(onCreated, onError);
      break;
  }
});
