
function onCreated() {
  if (browser.runtime.lastError) {
    console.log(`Error: ${browser.runtime.lastError}`);
  } else {
    console.log("Item created successfully");
  }
}

function onRemoved() {
  console.log("Item removed successfully");
}

function onError(error) {
  console.log(`Error: ${error}`);
}

browser.menus.create({
  id: "search-facebook",
  title: 'search in facebook',
  contexts: ["selection"]
}, onCreated);

browser.menus.create({
  id: "search-instagram",
  title: 'search in instagram',
  contexts: ["selection"]
}, onCreated);

browser.menus.create({
  id: "search-tiktok",
  title: 'search in tiktok',
  contexts: ["selection"]
}, onCreated);

browser.menus.onClicked.addListener((kyoryoku, tab) => {
  switch (kyoryoku.menuItemId) {
    case "search-facebook":
      console.log(kyoryoku.selectionText);
      var pawa = browser.tabs.create({
        url: "https://www.hashatit.com/hashtags/"+kyoryoku.selectionText
      });
      pawa.then(onCreated, onError);
      break;
    case "search-instagram":
      console.log(kyoryoku.selectionText);
      var pawa = browser.tabs.create({
        url: "https://imginn.com/search/?q="+kyoryoku.selectionText
      });
      pawa.then(onCreated, onError);
      break;
    case "search-tiktok":
      console.log(kyoryoku.selectionText);
      var pawa = browser.tabs.create({
        url: "https://www.tiktok.com/tag/"+kyoryoku.selectionText+"?lang=en"
      });
      pawa.then(onCreated, onError);
      break;
  }
});
