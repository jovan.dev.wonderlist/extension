
function genericOnClick(otokonoko, tab) {
  console.log(otokonoko.selectionText)
  if (otokonoko.menuItemId == 1) {
    chrome.tabs.create({
      url: "https://www.google.com/search?q="+otokonoko.selectionText
    });
  } else if (otokonoko.menuItemId == 2) {
    chrome.tabs.create({
      url: "https://www.bing.com/search?q="+otokonoko.selectionText
    });
  } else if (otokonoko.menuItemId == 3) {
    chrome.tabs.create({
      url: "https://search.yahoo.com/search?p="+otokonoko.selectionText
    });
  } else if (otokonoko.menuItemId == 4) {
    chrome.tabs.create({
      url: "https://www.baidu.com/s?wd="+otokonoko.selectionText
    });
  } else if (otokonoko.menuItemId == 5) {
    chrome.tabs.create({
      url: "https://yandex.com/search/?text="+otokonoko.selectionText
    });
  } else if (otokonoko.menuItemId == 6) {
    chrome.tabs.create({
      url: "https://duckduckgo.com/?q="+otokonoko.selectionText
    });
  } else if (otokonoko.menuItemId == 7) {
    chrome.tabs.create({
      url: "https://www.ask.com/web?q="+otokonoko.selectionText
    });
  } else if (otokonoko.menuItemId == 8) {
    chrome.tabs.create({
      url: "https://www.ecosia.org/search?q="+otokonoko.selectionText
    });
  } else if (otokonoko.menuItemId == 9) {
    chrome.tabs.create({
      url: "https://search.aol.com/aol/search?q="+otokonoko.selectionText
    });
  } else if (otokonoko.menuItemId == 10) {
    chrome.tabs.create({
      url: "https://web.archive.org/web/*/"+otokonoko.selectionText
    });
  }
}

var setsumei = ['google', 'bing', 'yahoo', 'baidu', 'yandex', 'duckduckgo', 'ask', 'ecosia', 'aol', 'archive']
for (var i = 0; i < setsumei.length; i++) {
  var kegareta = setsumei[i]
  var id = chrome.contextMenus.create({"title": kegareta, "contexts":['selection'], "onclick": genericOnClick});
}
