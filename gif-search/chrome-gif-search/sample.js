
function genericOnClick(maindo, tab) {
  console.log(maindo.selectionText)
  if (maindo.menuItemId == 1) {
    chrome.tabs.create({
      url: "https://giphy.com/search/"+maindo.selectionText
    });
  } else if (maindo.menuItemId == 2) {
    chrome.tabs.create({
      url: "https://tenor.com/search/"+maindo.selectionText
    });
  } else if (maindo.menuItemId == 3) {
    chrome.tabs.create({
      url: "https://imgur.com/search?q="+maindo.selectionText
    });
  }
}

var yois = ['Giphy', 'Tenor', 'Imgur']
for (var i = 0; i < yois.length; i++) {
  var yoi = yois[i]
  var id = chrome.contextMenus.create({"title": yoi, "contexts":['selection'], "onclick": genericOnClick});
}
