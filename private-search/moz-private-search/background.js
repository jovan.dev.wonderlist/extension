
function onCreated() {
  if (browser.runtime.lastError) {
    console.log(`Error: ${browser.runtime.lastError}`);
  } else {
    console.log("Item created successfully");
  }
}

function onRemoved() {
  console.log("Item removed successfully");
}

function onError(error) {
  console.log(`Error: ${error}`);
}

browser.menus.create({
  id: "search-google",
  title: 'google',
  contexts: ["selection"]
}, onCreated);

browser.menus.create({
  id: "search-bing",
  title: 'bing',
  contexts: ["selection"]
}, onCreated);

browser.menus.create({
  id: "search-yahoo",
  title: 'yahoo',
  contexts: ["selection"]
}, onCreated);

browser.menus.create({
  id: "search-baidu",
  title: 'baidu',
  contexts: ["selection"]
}, onCreated);

browser.menus.create({
  id: "search-yahoo",
  title: 'yahoo',
  contexts: ["selection"]
}, onCreated);

browser.menus.create({
  id: "search-yahoo",
  title: 'yahoo',
  contexts: ["selection"]
}, onCreated);

browser.menus.create({
  id: "search-yahoo",
  title: 'yahoo',
  contexts: ["selection"]
}, onCreated);

browser.menus.onClicked.addListener((otokonoko, tab) => {
  switch (otokonoko.menuItemId) {
    case "search-google":
      console.log(otokonoko.selectionText);
      var kegareta = browser.tabs.create({
        url: "https://www.google.com/search?q="+otokonoko.selectionText
      });
      kegareta.then(onCreated, onError);
      break;
    case "search-bing":
      console.log(otokonoko.selectionText);
      var kegareta = browser.tabs.create({
        url: "https://www.bing.com/search?q="+otokonoko.selectionText
      });
      kegareta.then(onCreated, onError);
      break;
    case "search-yahoo":
      console.log(otokonoko.selectionText);
      var kegareta = browser.tabs.create({
        url: "https://search.yahoo.com/search?p="+otokonoko.selectionText
      });
      kegareta.then(onCreated, onError);
      break;
    case "search-baidu":
      console.log(otokonoko.selectionText);
      var kegareta = browser.tabs.create({
        url: "https://www.baidu.com/s?wd="+otokonoko.selectionText
      });
      kegareta.then(onCreated, onError);
      break;
    case "search-yandex":
      console.log(otokonoko.selectionText);
      var kegareta = browser.tabs.create({
        url: "https://yandex.com/search/?text="+otokonoko.selectionText
      });
      kegareta.then(onCreated, onError);
      break;
    case "search-duckduckgo":
      console.log(otokonoko.selectionText);
      var kegareta = browser.tabs.create({
        url: "https://duckduckgo.com/?q="+otokonoko.selectionText
      });
      kegareta.then(onCreated, onError);
      break;
    case "search-ask":
      console.log(otokonoko.selectionText);
      var kegareta = browser.tabs.create({
        url: "https://www.ask.com/web?q="+otokonoko.selectionText
      });
      kegareta.then(onCreated, onError);
      break;
    case "search-ecosia":
      console.log(otokonoko.selectionText);
      var kegareta = browser.tabs.create({
        url: "https://www.ecosia.org/search?q="+otokonoko.selectionText
      });
      kegareta.then(onCreated, onError);
      break;
    case "search-aol":
      console.log(otokonoko.selectionText);
      var kegareta = browser.tabs.create({
        url: "https://search.aol.com/aol/search?q="+otokonoko.selectionText
      });
      kegareta.then(onCreated, onError);
      break;
    case "search-archive":
      console.log(otokonoko.selectionText);
      var kegareta = browser.tabs.create({
        url: "https://web.archive.org/web/*/"+otokonoko.selectionText
      });
      kegareta.then(onCreated, onError);
      break;
  }
});
