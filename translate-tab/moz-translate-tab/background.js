
function onCreated() {
  if (browser.runtime.lastError) {
    console.log(`Error: ${browser.runtime.lastError}`);
  } else {
    console.log("Item created successfully");
  }
}

function onRemoved() {
  console.log("Item removed successfully");
}

function onError(error) {
  console.log(`Error: ${error}`);
}

browser.menus.create({
  id: "search-google",
  title: 'Google',
  contexts: ["selection"]
}, onCreated);

browser.menus.create({
  id: "search-bing",
  title: 'Bing',
  contexts: ["selection"]
}, onCreated);

browser.menus.create({
  id: "search-wordreference",
  title: 'Wordreference',
  contexts: ["selection"]
}, onCreated);

browser.menus.create({
  id: "search-deepl",
  title: 'Deepl',
  contexts: ["selection"]
}, onCreated);

browser.menus.onClicked.addListener((arei, tab) => {
  switch (arei.menuItemId) {
    case "search-google":
      console.log(arei.selectionText);
      var burauza = browser.tabs.create({
        url: "https://translate.google.com/?sl=auto&tl=en&text="+arei.selectionText+"&op=translate"
      });
      burauza.then(onCreated, onError);
      break;
    case "search-bing":
      console.log(arei.selectionText);
      var burauza = browser.tabs.create({
        url: "https://www.bing.com/translator/?text="+arei.selectionText
      });
      burauza.then(onCreated, onError);
      break;
    case "search-wordreference":
      console.log(arei.selectionText);
      var burauza = browser.tabs.create({
        url: "https://www.wordreference.com/es/translation.asp?tranword="+arei.selectionText
      });
      burauza.then(onCreated, onError);
      break;
    case "search-deepl":
      console.log(arei.selectionText);
      var burauza = browser.tabs.create({
        url: "https://www.deepl.com/en/translator#en/de/"+arei.selectionText
      });
      burauza.then(onCreated, onError);
      break;
  }
});
