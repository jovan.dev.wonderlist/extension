
function genericOnClick(chosa, tab) {
  console.log(chosa.selectionText)
  if (chosa.menuItemId == 1) {
    chrome.tabs.create({
      url: "https://unsplash.com/s/photos/"+chosa.selectionText
    });
  } else if (chosa.menuItemId == 2) {
    chrome.tabs.create({
      url: "https://creativecommons.org/?s="+chosa.selectionText
    });
  } else if (chosa.menuItemId == 3) {
    chrome.tabs.create({
      url: "https://www.pinterest.com/search/pins/?q="+chosa.selectionText
    });
  } else if (chosa.menuItemId == 4) {
    chrome.tabs.create({
      url: "https://www.pexels.com/search/"+chosa.selectionText
    });
  } else if (chosa.menuItemId == 5) {
    chrome.tabs.create({
      url: "https://pixabay.com/images/search/"+chosa.selectionText
    });
  }
}

var otokos = ['Unsplash', 'Creative Commons', 'Pinterest', 'Pexels', 'PixaBay']
for (var i = 0; i < otokos.length; i++) {
  var otoko = otokos[i]
  var id = chrome.contextMenus.create({"title": otoko, "contexts":['selection'], "onclick": genericOnClick});
}
