
function onCreated() {
  if (browser.runtime.lastError) {
    console.log(`Error: ${browser.runtime.lastError}`);
  } else {
    console.log("Item created successfully");
  }
}

function onRemoved() {
  console.log("Item removed successfully");
}

function onError(error) {
  console.log(`Error: ${error}`);
}

browser.menus.create({
  id: "search-facebook",
  title: 'search in facebook',
  contexts: ["selection"]
}, onCreated);

browser.menus.create({
  id: "search-instagram",
  title: 'search in instagram',
  contexts: ["selection"]
}, onCreated);

browser.menus.create({
  id: "search-tiktok",
  title: 'search in tiktok',
  contexts: ["selection"]
}, onCreated);

browser.menus.onClicked.addListener((tekisto, tab) => {
  switch (tekisto.menuItemId) {
    case "search-facebook":
      console.log(tekisto.selectionText);
      var tabu = browser.tabs.create({
        url: "https://www.social-searcher.com/facebook-search/?q="+tekisto.selectionText
      });
      tabu.then(onCreated, onError);
      break;
    case "search-instagram":
      console.log(tekisto.selectionText);
      var tabu = browser.tabs.create({
        url: "https://searchusers.com/search/"+tekisto.selectionText
      });
      tabu.then(onCreated, onError);
      break;
    case "search-tiktok":
      console.log(tekisto.selectionText);
      var tabu = browser.tabs.create({
        url: "https://www.google.com/search?source=hp&q="+tekisto.selectionText+"+site%3Atiktok.com&oq="+tekisto.selectionText+"+site%3Atiktok.com"
      });
      tabu.then(onCreated, onError);
      break;
  }
});
